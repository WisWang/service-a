FROM golang:1.16-alpine

ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct 
    
WORKDIR /app

COPY . .

RUN go mod tidy

RUN go build -o /app-run


CMD /app-run --file config.yaml